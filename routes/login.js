var express = require('express');
var router = express.Router();

/* GET login page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Login page ' });
});


router.post('/',function(req,res,next){
  var db = req.con;
  var user_name = req.body.inputEmail
  var user_password = req.body.inputPassword
  

  db.query('SELECT userpassword,user_status FROM user WHERE username = ?',[user_name], function(err, rows) {
        if (err) {
            console.log(err);
        }

        var data = rows;
        
        if (user_password==data[0].userpassword){
          if(data[0].user_status==1){
            req.session.user_name = user_name
            res.render('index', {  user_name:req.session.user_name });
          }
          if(data[0].user_status==2){
            req.session.user_name = user_name
            res.render('publisher_index', {  user_name:req.session.user_name , title:"publisher_index"});
          }
          if(data[0].user_status==3){
            req.session.user_name = user_name
            var db = req.con;
            db.query('SELECT * FROM product WHERE status = "funding"',[req.session.user_name], function(err, rows){
            if (err) {
              console.log(err);
            }
            var data = rows;
            res.redirect('/BackendUserIndex/')
            //res.render('backend_user_index', { user_name: req.session.user_name,data:data });
            })
          }
        }
        else {
          res.send("login error occur")
        }
});
})


router.get('/logout',function(req,res,next){
  res.redirect('/login');
})




module.exports = router;
