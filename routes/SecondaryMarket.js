
var express = require('express');
var router = express.Router();
const Web3Functions = require('../Contracts/blockchain');
var Web3 = require('web3');
const testnet = 'https://ropsten.infura.io/v3/a541e066f03d4654b032bf76f1442979';
const web3 = new Web3(new Web3.providers.HttpProvider(testnet)); //串以太坊


/* GET second market page. */
router.get('/', function(req, res, next) {
  if(req.session.user_name){
  var db = req.con;
  db.query(`
  SELECT sec_market.symbol,sec_market.seller,sec_market.quantity,sec_market.price ,product.img,product.intro
  FROM sec_market
  Join product
  ON sec_market.symbol=product.symbol
  `, function(err, rows) {
    if (err) {
        console.log(err);
    }

    var data = rows;
    res.render('SecondaryMarket', {data:data} );
});}
else {
  res.send("please login first")
}
  
});




router.post('/publish',async function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol
    var quantity = req.body.inputQuantity
    var price = req.body.inputPrice
    var seller = req.session.user_name
    var token_owner = await UserAddr(db,seller)
    var contract_addr = await ContractAddr(db,symbol)
    //這邊尚未實作：用web3向seller發起allowance request，再來用seller PK去approveAllowance
    db.query("SELECT * FROM user WHERE username = ?",[req.session.user_name],async function(err,rows){
      if(err){
        res.send(err);
      }
      await Web3Functions.PendingSecOrder(rows[0].user_eth_addr,price,quantity,contract_addr,rows[0].user_pk).then(function(error){
        if(error){
          res.send(error.message);
        }
        else{
          db.query("INSERT INTO `sec_market` (`symbol`, `seller`, `quantity`, `price`) VALUES (?, ?, ?, ?);"
          ,[symbol,seller,quantity,price],function(err){
            if(err){
              console.log(err);
            }
            else{
              res.redirect('/SecondaryMarket');
            }
          })
        }
      });
    })
    
  }
})


router.post('/order',async function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol
    var quantity = req.body.inputQuantity
    var price = req.body.inputPrice
    var buyer = req.session.user_name
    //var PK = req.body.inputPK
    var seller = req.body.inputSeller
    var buyer_addr = await UserAddr(db,buyer)
    var seller_addr = await UserAddr(db,seller)
    var contract_addr = await ContractAddr(db,symbol)
    //先transfer eth token 給 seller
    db.query("SELECT * FROM user WHERE username = ?",[req.session.user_name],async function(err,rows){
    if(1==1){
      //成功即transfer token    
      await Web3Functions.SecOrderPurchase(buyer_addr,seller_addr,price*quantity,quantity,contract_addr,rows[0].user_pk);
      //更改資料庫 1.將掛單下下來
      var remain = await GetBidRemain(db,symbol,seller);
      if(remain-quantity==0){ //完全購買完畢
        db.query("DELETE FROM `sec_market` WHERE (`symbol` = ?) and (`seller` = ?);",[symbol,seller],async function(err){
          if(err){
            res.send(err);
          }
          await SetMyAsset(db,symbol,"+",buyer,quantity);//更改資料庫 2.新增buyer新買的myasset
          await SetMyAsset(db,symbol,"-",seller,quantity);//更改資料庫 3.刪減seller賣掉的myasset
        })
      }
      else if(remain-quantity>0){ //購買後尚有剩下
        db.query("UPDATE `sec_market` SET `quantity` = ? WHERE (`symbol` = ?) and (`seller` = ?);",[remain-quantity,symbol,seller],async function(err){
          if(err){
            res.send(err);
          }
          await SetMyAsset(db,symbol,"+",buyer,quantity);//更改資料庫 2.新增buyer新買的myasset
          await SetMyAsset(db,symbol,"-",seller,quantity);//更改資料庫 3.刪減seller賣掉的myasset
        })
      }
      res.redirect("/SecondaryMarket")
    }
    else{
      res.send("payment fail..")
    }
    })
    
  }
})



router.post('/cancel',async function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol
    var seller = req.session.user_name
    var contract_addr = await ContractAddr(db,symbol)
    //這邊尚未實作：用web3向seller發起allowance request，再來用seller PK去approveAllowance
    db.query("SELECT * FROM user WHERE username = ?",[req.session.user_name],async function(err,rows){
      if(err){
        res.send(err);
      }
      await Web3Functions.SecOrderCancel(rows[0].user_eth_addr,contract_addr,rows[0].user_pk).then(function(error){
        if(error){
          res.send(error.message);
        }
        else{
          db.query("DELETE FROM `demo`.`sec_market` WHERE (`symbol` = ?) and (`seller` = ?);"
          ,[symbol,seller],function(err){
            if(err){
              console.log(err);
            }
            else{
              res.redirect('/SecondaryMarket');
            }
          })
        }
      });
    })
    
  }
})



async function UserAddr(db,username){
  return new Promise(function(resolve,reject){
  db.query('SELECT user_eth_addr FROM user WHERE username=?',[username],function(err,rows){
    if(err){
      reject(err);
    }
    else{
      resolve(rows[0].user_eth_addr);
    }
  })})
}

async function ContractAddr(db,symbol){
  return new Promise(function(resolve,reject){
  db.query('SELECT eth_addr FROM product WHERE symbol=?',[symbol],function(err,rows){
    if(err){
      reject(err); 
    }
    else{
      resolve(rows[0].eth_addr);
    }
  })})
}


async function user_pay(user_address,user_key,seller_addr,total_price){
  return new Promise(function(resolve,reject){
              var wallet_address = user_address;// 轉出者ID
              var count = web3.eth.getTransactionCount(wallet_address);
              var rawTransacton = {
	              "from": wallet_address,
	              "nonce": web3.toHex(count),
	              "gasPrice": web3.toHex(21000000000),
	              "gasLimit": web3.toHex(200000),
	              "to":seller_addr,
                "value": web3.toHex(total_price*10000000000000) //小於一以太
              };
              var Tx = require('ethereumjs-tx').Transaction;
              var privateKey = Buffer.from(user_key,"hex");// 此處需要連接個人的私鑰
              var tx = new Tx(rawTransacton,{"chain":"ropsten"});     
              tx.sign(privateKey);
              var serializedTx = tx.serialize();
              web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){
                if(!err){
                  console.log("https://ropsten.etherscan.io/tx/"+hash);
                  console.log("buyer payment complete!")
                  resolve(true);
                }
                else{
                  reject(err);
                }
              });
              });
              
            
};

async function GetBidRemain(db,symbol,seller){
  return new Promise(function(resolve,reject){
    db.query('SELECT quantity FROM sec_market WHERE symbol=? AND seller=?;',[symbol,seller],function(err,rows){
        if(err){
          reject(err);
        }
        else{
          console.log(rows[0].quantity)
          resolve(rows[0].quantity)
        }
    })
  })
}

async function GetMyAssetRemain(db,symbol,owner){
  return new Promise(function(resolve,reject){
    db.query('SELECT quantity FROM myasset WHERE asset_id=? AND user_id=?;',[symbol,owner],function(err,rows){
        if(err){
          reject(err);
        }
        else{
          try{
            resolve(rows[0].quantity)
          }
          catch(err){
            resolve(0);  
          }
        }
    })
  })
}

async function SetMyAsset(db,symbol,method,target_user,delta_quantity){
  return new Promise( async function(resolve,reject){
    //查看原本該symbol剩餘資產
    var remain = await GetMyAssetRemain(db,symbol,target_user);
    //判斷是要增加還是減少資產
    if(method=="+"){
      //進行SQL語法
      if(remain>0){
        db.query("UPDATE `myasset` SET `quantity` = ? WHERE (`user_id` = ?) and (`asset_id` = ?);",[parseInt(remain)+parseInt(delta_quantity),target_user,symbol],function(err){
          if(err){
            reject(err);
          }
          else{
            resolve();
          }
        })
      }
      else{
        db.query("INSERT INTO `myasset` (`user_id`, `asset_id`, `quantity`) VALUES (?, ?, ?);",[target_user,symbol,delta_quantity],function(err){
          if(err){
            reject(err);
          }
          else{
            resolve();
          }
        })
      }
    }
    else{
      //進行SQL語法
      if(remain-delta_quantity>0){
        db.query("UPDATE `myasset` SET `quantity` = ? WHERE (`user_id` = ?) and (`asset_id` = ?);",[parseInt(remain)-parseInt(delta_quantity),target_user,symbol],function(err){
          if(err){
            reject(err);
          }
          else {
            resolve();  
          }
        })
      }
      else{
        db.query("DELETE FROM `myasset` WHERE (`asset_id` = ?) and (`user_id` = ?);",[symbol,target_user],function(err){
          if(err){
            reject(err);
          }
          else {
            resolve();  
          }
        })
      }
    }
  })
}




module.exports = router;
