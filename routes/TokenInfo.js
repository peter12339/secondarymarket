var express = require('express');
var router = express.Router();
var Web3 = require('web3');
const testnet = 'https://ropsten.infura.io/v3/a541e066f03d4654b032bf76f1442979';
const web3 = new Web3(new Web3.providers.HttpProvider(testnet)); //串以太坊


/* GET TokenInfo page. */
router.get('/', function(req, res, next) {
  if(req.session.user_name){
  var db = req.con;
  db.query('SELECT * FROM product WHERE status = "funding"', function(err, rows) {
    if (err) {
        console.log(err);
    }
    var data = rows;
    db.query('SELECT * FROM product WHERE status = "mintmore_funding"',function(err,rows){
      if(err){
        send(err);
      }
      else {
        res.render('TokenInfo', {data:data,mint_data:rows} );
      }
    })
});}
else {
  res.send("please login first")
}
  
});
//user1 addr is here 0x05EB4951f821915371226C15b35a372003Ae0517
//user1 PK is here 9EA2AB317A17E8CE82AAC71F56C2BEDF7DC8AD590F6F27351B5FB6FF5BED116D
//user2 addr is here 0x4e8Da630F672ecD0fE018f1dA71e456300Ed3F57
//user2 PK is here a9e0abb0115a86b1ea1fe9a81856b60e247ec7fbdce9c0613cee851728333b32
router.post('/purchase',function(req, res, next){
  
  if(req.session.user_name){
    var db = req.con;
    db.query('SELECT * FROM user WHERE username=?',[req.session.user_name],function(err,rows){
      if(err){
        console.log(err);
        res.send("address is not found in database.")
      }
      var user_address = rows;
      if(check_balance_available(user_address[0].user_eth_addr,req.body.inputQuantity,req.body.inputPrice)){
        if (user_pay(db,user_address[0].user_eth_addr,user_address[0].user_pk,req.body.inputSymbol,req.body.inputQuantity*req.body.inputPrice)){
          write_order_list(db,req.session.user_name,req.body.inputSymbol,req.body.inputQuantity);
          res.redirect('/');
        }
        else{
          res.send("User payment process error!");
        }
      }
      else{
        res.send("Balance is not sufficient.");
      }
    })
    
  }
});



function check_balance_available(user_address,quantity,price){
    //這邊加入web3js語法
    var wallet_address = user_address;
    var eth = web3.eth.getBalance(wallet_address);
    eth = web3.toDecimal(eth);
    eth = web3.fromWei(eth,"ether");
    if(quantity*price*0.00001<eth){
      return true;
    }
    else{
      return false;
    }
}

function user_pay(db,user_address,user_key,symbol,total_price){
  db.query(`SELECT product.symbol , user.user_pk,user.user_eth_addr
            FROM demo.product
            INNER JOIN demo.user
            ON product.publisher=user.username
            WHERE product.symbol=?`
            ,[symbol]
            ,function (err, rows){
              if(err){
                console.log(err);
              }
              var wallet_address = user_address;// 轉出者ID
              var to_address = rows[0].user_eth_addr;// 接收者ID
              var count = web3.eth.getTransactionCount(wallet_address);
              var rawTransacton = {
	              "from": wallet_address,
	              "nonce": web3.toHex(count),
	              "gasPrice": web3.toHex(21000000000),
	              "gasLimit": web3.toHex(200000),
	              "to":to_address,
                "value": web3.toHex(total_price*10000000000000)
              };
              var Tx = require('ethereumjs-tx').Transaction;
              var privateKey = Buffer.from(user_key,"hex");// 此處需要連接個人的私鑰
              var tx = new Tx(rawTransacton,{"chain":"ropsten"});     
              tx.sign(privateKey);
              var serializedTx = tx.serialize();
              web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){
                if(!err){
                  console.log("https://ropsten.etherscan.io/tx/"+hash);
                }
                else{
                  console.log(err);
                }
              });
              });
              return true;
};


function write_order_list(db,user_name,symbol,quantity){
  db.query("INSERT INTO order_list (order_user, order_product, order_amount) VALUES (?, ?, ?);",[user_name,symbol,quantity]);
  db.query("SELECT quantity FROM demo.product WHERE symbol=? ;",[symbol],function(err,rows){
    if (err){
      console.log(err);
    }
    else{
      db.query("UPDATE product SET quantity = ? WHERE (symbol = ?);",[rows[0].quantity-quantity,symbol],function (err){
        console.log(err);
      })
    }
  });
}




module.exports = router;
