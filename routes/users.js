var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/MyAsset', function(req, res, next) {
  if(req.session.user_name){
  var db = req.con;
  db.query('SELECT myasset.asset_id, myasset.user_id,myasset.quantity,product.name,product.rights,product.total_supply FROM demo.myasset INNER Join demo.product ON myasset.asset_id=product.symbol Where user_id=?;',[req.session.user_name], function(err, rows){
    if (err) {
      console.log(err);
    }
    var data = rows;
    res.render('MyAsset', { user_name: req.session.user_name,data:data });
  })}
  else{
    res.send("please login first")
  }
  
});

router.get('/purchase', function(req, res, next) {
  if(req.session.user_name){
    
    res.render('purchase', { title: 'purchase page ' });
  }
});






module.exports = router;
