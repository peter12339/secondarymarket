var express = require('express');
var router = express.Router();

/* GET registry page. */
router.get('/', function(req, res, next) {
  res.render('registry', { title: 'registry page ' });
});

router.get('/user', function(req, res, next) {
  res.render('user_registry', { title: 'registry page ' });
});


router.get('/publisher', function(req, res, next) {
  res.render('publisher_registry', { title: 'registry page ' });
});







module.exports = router;
