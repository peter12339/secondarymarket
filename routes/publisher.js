var express = require('express');
var router = express.Router();
const Web3Functions = require('../Contracts/blockchain');
var Web3 = require('web3');
const session = require('express-session');
const testnet = 'https://ropsten.infura.io/v3/a541e066f03d4654b032bf76f1442979';
const web3 = new Web3(new Web3.providers.HttpProvider(testnet)); //串以太坊




/* GET order page. */
router.get('/', function(req, res, next) {
  if(req.session.user_name){
    res.render('publisher_index', {user_name:req.session.user_name});
  }
  else {
    res.send("please login first")
  }
});


router.get('/publish', function(req, res, next) {
  if(req.session.user_name){
    res.render('publish', {user_name:req.session.user_name});
  }
  else {
    res.send("please login first")
  }
  
});

router.post('/publish',function (req,res,next){
  var db = req.con;
  var product_name = req.body.name
  var product_quantity = req.body.quantity
  var product_price = req.body.price
  var product_info = req.body.Remarks
  var product_publisher = req.session.user_name

  db.query("INSERT INTO product ( `name`, `status`, `quantity`, `intro`,`img`,`price`,`publisher`,`rights`,`total_supply`) VALUES (?,'draft',?,?,'/images/shutterstock_548079301.jpg',?,?,?,?);",[product_name,product_quantity,product_info,product_price,product_publisher,product_quantity,product_quantity],function(err){
    if (err){console.log(err)}
    res.redirect('/publisher')
  })
});


router.get('/MyPublish',function(req,res,next){
  if(req.session.user_name){
    var db =req.con;
    db.query("SELECT * FROM product WHERE status='funding_over' AND publisher=?",[req.session.user_name],function(err,rows){
      if(err){
        res.send(err);
      }
      else{
        var data = rows;
        db.query("SELECT * FROM product WHERE status='mintmore_funding' AND publisher=?",[req.session.user_name],function(err,rows){
          if(err){
            res.send(err);
          }
          else{
            var data_funding = rows
            db.query("SELECT product.symbol, product.name,product.publisher,product.quantity,MinterEvent.minter_event_username FROM demo.product INNER Join demo.MinterEvent ON product.symbol=MinterEvent.minter_event_symbol Where minter_event_username=?;",[req.session.user_name],function(err,rows){
              if(err){
                res.send(err);
              }
              else {
                res.render('mintmore',{user_name:req.session.user_name,data:data,data_funding:data_funding,Minter:rows});
              }
            })
          }

        })
      }
    })
  }
  else{
    res.send("Please login first.")
  }
})


router.post('/MintDraft',function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var name = req.body.inputName;
    var symbol = req.body.inputSymbol;
    var quantity = req.body.inputQuantity;
    db.query("INSERT INTO mint_request ( `mint_symbol`,`mint_quantity`,`mint_name`,`mint_status`) VALUES (?,?,?,'unapproved');",[symbol,quantity,name],function(err){
      if(err){
        res.send(err);
      }
      else {
        res.redirect('/publisher');
      }
    })    
  }
})

router.post('/Minting',async function(req,res,next){
  if(req.session.user_name){
    var db =req.con;
    var symbol = req.body.inputSymbol;
    var name = req.body.inputName;
    var quantity =req.body.inputQuantity;
    db.query("SELECT * FROM product WHERE name = ? ",[name],async function(err,rows){
      if(err){
        res.send(err);
      }
      else{
        var data= rows;
        db.query("SELECT * FROM user WHERE username=?",[req.session.user_name],async function(err,rows){
          if(err){res.send(err)};
          var data1 = rows
          await Web3Functions.RightsMint(data1[0].user_eth_addr,quantity,data[0].eth_addr,data1[0].user_pk);
        })
        db.query("UPDATE `demo`.`product` SET `rights` = ? WHERE (`symbol` = ?);",[parseInt(data[0].rights)+parseInt(quantity),symbol],async function(err){
          if(err){
            res.send(err)
          }
          res.redirect('/publisher');
        })
      }
    })
    
  }
})

router.post("/BurnRequest",async function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    var name = req.body.inputName;
    var quantity = req.body.inputQuantity;
    db.query("INSERT INTO `demo`.`burn_request` (`burn_symbol`, `burn_quantity`,`burn_status`,`burn_name`) VALUES (?, ?,'unapproved',?);",[symbol,quantity,name],function(err){
      if(err){
        res.send(err);
      }
      else{
        res.redirect('/');
      }
    })
  }
  else{
    res.send("please login first...")
  }
})


router.post('/RightsBurn',async function(req, res){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    var quantity = req.body.inputQuantity;
    var name = req.body.inputName;
    db.query("SELECT * FROM user WHERE username=?",[req.session.user_name],async function(err,rows){
      if(err){
        res.send('SQL query failed...');
      }
      else{
        var data  = rows;
        db.query("SELECT * FROM product WHERE symbol = ?",[symbol],async function(err,rows){
          if(err){
            res.send(err);
          }
          else{
            await Web3Functions.RightsBurn(data[0].user_eth_addr,quantity,rows[0].eth_addr,data[0].user_pk);
            db.query("UPDATE `demo`.`product` SET `rights` = ? WHERE (`symbol` = ?);",[rows[0].rights-quantity,symbol])
            res.redirect('/publisher/MyPublish');
          }
        })
        
      }
    })
  }
})





router.post('/AddMinterRequest',async function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    var Minter = req.body.inputMinter;
    db.query("INSERT INTO `demo`.`MinterEvent` (`minter_event_symbol`, `minter_event_username`, `minter_event_content`, `minter_event_status`) VALUES (?, ?, '+', 'unapproved');",[symbol,Minter],function(err){
      if(err){
        res.send(err);
      }
      else{
        res.redirect("/publisher");
      }
    })
  }
  else{
    res.send("please login first...");
  }
})



router.post('/RemoveMinterRequest',async function(req,res,next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    var Minter = req.body.inputMinter;
    db.query("INSERT INTO `demo`.`MinterEvent` (`minter_event_symbol`, `minter_event_username`, `minter_event_content`, `minter_event_status`) VALUES (?, ?, '-', 'unapproved');",[symbol,Minter],function(err){
      if(err){
        res.send(err);
      }
      else{
        res.redirect("/publisher");
      }
    })
  }
  else{
    res.send("please login first...");
  }
})


async function SetMyAsset(db,symbol,method,target_user,delta_quantity){
  return new Promise( async function(resolve,reject){
    //查看原本該symbol剩餘資產
    var remain = await GetMyAssetRemain(db,symbol,target_user);
    //判斷是要增加還是減少資產
    if(method=="+"){
      //進行SQL語法
      if(remain>0){
        db.query("UPDATE `myasset` SET `quantity` = ? WHERE (`user_id` = ?) and (`asset_id` = ?);",[parseInt(remain)+parseInt(delta_quantity),target_user,symbol],function(err){
          if(err){
            reject(err);
          }
          else{
            resolve();
          }
        })
      }
      else{
        db.query("INSERT INTO `myasset` (`user_id`, `asset_id`, `quantity`) VALUES (?, ?, ?);",[target_user,symbol,delta_quantity],function(err){
          if(err){
            reject(err);
          }
          else{
            resolve();
          }
        })
      }
    }
    else{
      //進行SQL語法
      if(remain-delta_quantity>0){
        db.query("UPDATE `myasset` SET `quantity` = ? WHERE (`user_id` = ?) and (`asset_id` = ?);",[parseInt(remain)-parseInt(delta_quantity),target_user,symbol],function(err){
          if(err){
            reject(err);
          }
          else {
            resolve();  
          }
        })
      }
      else{
        db.query("DELETE FROM `myasset` WHERE (`asset_id` = ?) and (`user_id` = ?);",[symbol,target_user],function(err){
          if(err){
            reject(err);
          }
          else {
            resolve();  
          }
        })
      }
    }
  })
}

async function GetMyAssetRemain(db,symbol,owner){
  return new Promise(function(resolve,reject){
    db.query('SELECT quantity FROM myasset WHERE asset_id=? AND user_id=?;',[symbol,owner],function(err,rows){
        if(err){
          reject(err);
        }
        else{
          try{
            resolve(rows[0].quantity)
          }
          catch(err){
            resolve(0);  
          }
        }
    })
  })
}

module.exports = router;
