var express = require('express');
var router = express.Router();
const Web3Functions = require('../Contracts/blockchain');
var Web3 = require('web3');
const testnet = 'https://ropsten.infura.io/v3/a541e066f03d4654b032bf76f1442979';
const web3 = new Web3(new Web3.providers.HttpProvider(testnet)); //串以太坊



const{Deployment} = require('../Contracts/contractDeployment');

/* GET order page. */
router.get('/', function(req, res, next) {
  if(req.session.user_name){
    var db = req.con;
    db.query('SELECT * FROM product WHERE status = "funding"',[req.session.user_name], function(err, rows){
    if (err) {
      console.log(err);
    }
    var data = rows;
    db.query('SELECT * FROM product WHERE status = "draft"',function(err, rows){
      if(err){
        res.send(err);
      }
      else{
        var data2 = rows;
        db.query('SELECT * FROM mint_request WHERE mint_status="unapproved"',function(err,rows){
          if(err){res.send(err);}
          else{
            var Mintmore_data = rows
            db.query('SELECT * FROM burn_request WHERE burn_status = "unapproved"',function(err,rows){
              if(err){
                res.send(err);
              }
              else {
                var Burn_data = rows;
                db.query('SELECT * FROM MinterEvent WHERE minter_event_status="unapproved"',function(err,rows){
                  if(err){
                    res.send(err);
                  }
                  else{
                    var Minter_data = rows;
                    db.query('SELECT * FROM product WHERE status="funding_over"',function(err,rows){
                      if(err){
                        res.send(err);
                      }
                      else{
                        {
                          res.render('backend_user_index', { user_name: req.session.user_name,data:data ,draft_data:data2,Mintmore_data:Mintmore_data,Burn_data:Burn_data,Minter_data:Minter_data,Operating:rows});
                        }}
                    })
                  }
                })
              }
            })
          }
        })
        
      }
    })
    
  })}
  else{
    res.send("please login first");
  }
});

router.post('/approve',function(req, res, next){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    var TokenName = req.body.inputName;
    db.query("UPDATE `demo`.`product` SET `status` = 'funding_over' WHERE (`symbol` = ?)",[symbol],function(err){ //更改產品狀態
      if(err){
        console.log(err);
        res.send(err);
      }
    });
    console.log("Symbol: "+symbol+"   DB: product status更改完畢!");
        //DB: 依照order_list 更改合約, 像是Owner的部分,Balance的部分 該部分先實作完成但還沒實現完全的彈性
        db.query(`
        SELECT order_list.order_amount, order_list.order_product,user.user_eth_addr
        FROM demo.order_list
        INNER Join demo.user
        ON order_list.order_user=user.username
        Where order_product=?;
        `,[symbol],function(err,rows){
          if(err){
            res.send(err);
          }
          else{
            owner=[]
            balance=[]
            for (var i=0;i<rows.length;i++){
              owner.push(rows[i].user_eth_addr)
              balance.push(rows[i].order_amount)
            }
            Deployment(db,symbol,TokenName,"0x24aC70BD93DDEf8dd3Fa496A5A16250800F8198f",owner,balance);//進行contract deployment
          }
        })
    db.query("SELECT `order_user`,`order_amount` FROM `demo`.`order_list` WHERE (`order_product`=?)",[symbol],function(err,rows){
      if(err){
        console.log(err);
        res.send(err);
      }
      else{
        var data = rows;
        for(var i = 0 ; i < data.length ; i++){
          WriteToMyasset(db,symbol,data[i].order_user,data[i].order_amount);//將Order table的資料寫入myasset當中
        }
        console.log('DB: 已將order_list訂單更改至myasset');
      }
    })
    res.redirect('/BackendUserIndex');
  }
  else{
    res.send("please login first");
  }
})

router.post('/DraftApprove', function(req, res){
  var db =req.con;
  var symbol = req.body.inputSymbol;
  db.query("UPDATE `product` SET `status` = 'funding' WHERE (`symbol` = ?);",[symbol],function(err){
    if(err){
      res.send(err);
    }
    else{
      res.redirect('/BackendUserIndex');
    }
  })
})


router.post('/BurningApprove',async function(req, res){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    var quantity = req.body.inputQuantity;
    var name = req.body.inputName;
    db.query("SELECT * FROM user WHERE username=?",[req.session.user_name],async function(err,rows){
      if(err){
        res.send('SQL query failed...');
      }
      else{
        var data  = rows;
        db.query("SELECT * FROM product WHERE symbol = ?",[symbol],async function(err,rows){
          if(err){
            res.send(err);
          }
          else{
              await Web3Functions.ApproveForBurn(data[0].user_eth_addr,quantity,rows[0].eth_addr,data[0].user_pk);
            db.query("UPDATE `demo`.`burn_request` SET `burn_status` = 'approved' WHERE (`burn_symbol` = ? );",[symbol],function(err){
              if(err){
                console.log(err);
              }
            })
            res.redirect('/BackendUserIndex');
          }
        })
        
      }
    })
  }
  else{
    res.send("please login first...")
  }
})

router.post('/MintMoreApprove', async function(req, res){
  var db =req.con;
  var name = req.body.inputNmae;
  var quantity = req.body.inputQuantity;
  var symbol = req.body.inputSymbol;
  db.query("SELECT * FROM user WHERE username=?",[req.session.user_name],async function(err,rows){
    if(err){
      res.send(err);
    }
    else{
      var data = rows
      await db.query("SELECT * FROM product WHERE symbol=?",[symbol],async function(err,rows){
        if(err){
          res.send(err);
        }
        await Web3Functions.ApproveForMint(data[0].user_eth_addr,quantity,rows[0].eth_addr,data[0].user_pk);
      })
    }
  })
  db.query("UPDATE `mint_request` SET `mint_status` = 'approved' WHERE (`mint_symbol` = ?);",[symbol],function(err){
    if(err){
      res.send(err);
    }
    else{
      res.redirect('/BackendUserIndex');
    }
  })
})


router.post('/MinterApprove', async function(req,res,next){
  var db = req.con;
  var id = req.body.inputId
  var symbol = req.body.inputSymbol
  var user = req.body.inputUsername
  var content = req.body.inputContent
  if(content=="+"){
    db.query("SELECT MinterEvent.minter_event_symbol,MinterEvent.minter_event_content,MinterEvent.minter_event_status, MinterEvent.minter_event_username,user.user_eth_addr,product.eth_addr FROM (demo.MinterEvent INNER JOIN demo.user ON MinterEvent.minter_event_username=user.username) INNER JOIN demo.product ON MinterEvent.minter_event_symbol=product.symbol WHERE minter_event_symbol = ? AND minter_event_status='unapproved' AND minter_event_content = ?",[symbol,content],async function(err,rows){
      if(err){
        res.send(err);
      }
      else{
        try {
          target_addr=rows[0].user_eth_addr
          token_addr=rows[0].eth_addr
          await Web3Functions.AddRoleMember(target_addr,"Minters",token_addr);
          //將unapproved 改成 approved
          db.query("UPDATE `demo`.`MinterEvent` SET `minter_event_status` = 'approved' WHERE (`minter_event_id` = ?);",[id],async function(err){
            if(err){
              res.send(err);
            }
          })
          res.redirect('/BackendUserIndex');
        }
        catch (err){
          res.send(err);
        }
      }
    })
  }
  if(content=="-"){
    db.query("SELECT MinterEvent.minter_event_symbol,MinterEvent.minter_event_content,MinterEvent.minter_event_status, MinterEvent.minter_event_username,user.user_eth_addr,product.eth_addr FROM (demo.MinterEvent INNER JOIN demo.user ON MinterEvent.minter_event_username=user.username) INNER JOIN demo.product ON MinterEvent.minter_event_symbol=product.symbol WHERE minter_event_symbol = ? AND minter_event_status='unapproved' AND minter_event_content = ?",[symbol,content],async function(err,rows){
      if(err){
        res.send(err);
      }
      else{
        try {
          target_addr=rows[0].user_eth_addr
          token_addr=rows[0].eth_addr
          await Web3Functions.RemoveRoleMember(target_addr,"Minters",token_addr);
          //將刪除Minter權限 in db
          db.query("DELETE FROM `demo`.`MinterEvent` WHERE (`minter_event_symbol` = ? AND `minter_event_username` = ?);",[symbol,user],async function(err){
            if(err){
              res.send(err);
            }
          })
          res.redirect('/BackendUserIndex');
        }
        catch (err){
          res.send(err);
        }
      }
    })
  }
})





router.post("/GetContractAddress",async function(req,res){
  if(req.session.user_name){
    var db = req.con;
    var symbol = req.body.inputSymbol;
    db.query("SELECT * FROM demo.product WHERE symbol=?",[symbol],async function(err,rows){
      if(err){
        res.send(err);
      }
      else{
        var Address = await Web3Functions.getContractAddress(rows[0].eth_TxHash);
        db.query("UPDATE `demo`.`product` SET `eth_addr` = ? WHERE (`symbol` = ?);",[Address,symbol],function(err){
          if(err){
            res.send(err)
          }
          else{
            console.log("Contract Address insertion in db success")
            res.redirect('/BackendUserIndex')
          }
        })
      }
    })
  }
  else {
    res.send("please login first...")
  }
})











function WriteToMyasset(db,symbol,owner,amount){
  db.query('INSERT INTO `myasset` (`user_id`, `asset_id`,`quantity`) VALUES (?,?,?);',[owner,symbol,amount],function(err){
    if(err){
      console.log(err);
    }
  })
}



async function SetMyAsset(db,symbol,method,target_user,delta_quantity){
  return new Promise( async function(resolve,reject){
    //查看原本該symbol剩餘資產
    var remain = await GetMyAssetRemain(db,symbol,target_user);
    //判斷是要增加還是減少資產
    if(method=="+"){
      //進行SQL語法
      if(remain>0){
        db.query("UPDATE `myasset` SET `quantity` = ? WHERE (`user_id` = ?) and (`asset_id` = ?);",[parseInt(remain)+parseInt(delta_quantity),target_user,symbol],function(err){
          if(err){
            reject(err);
          }
          else{
            resolve();
          }
        })
      }
      else{
        db.query("INSERT INTO `myasset` (`user_id`, `asset_id`, `quantity`) VALUES (?, ?, ?);",[target_user,symbol,delta_quantity],function(err){
          if(err){
            reject(err);
          }
          else{
            resolve();
          }
        })
      }
    }
    else{
      //進行SQL語法
      if(remain-delta_quantity>0){
        db.query("UPDATE `myasset` SET `quantity` = ? WHERE (`user_id` = ?) and (`asset_id` = ?);",[parseInt(remain)-parseInt(delta_quantity),target_user,symbol],function(err){
          if(err){
            reject(err);
          }
          else {
            resolve();  
          }
        })
      }
      else{
        db.query("DELETE FROM `myasset` WHERE (`asset_id` = ?) and (`user_id` = ?);",[symbol,target_user],function(err){
          if(err){
            reject(err);
          }
          else {
            resolve();  
          }
        })
      }
    }
  })
}

async function GetMyAssetRemain(db,symbol,owner){
  return new Promise(function(resolve,reject){
    db.query('SELECT quantity FROM myasset WHERE asset_id=? AND user_id=?;',[symbol,owner],function(err,rows){
        if(err){
          reject(err);
        }
        else{
          try{
            resolve(rows[0].quantity)
          }
          catch(err){
            resolve(0);  
          }
        }
    })
  })
}





module.exports = router;
