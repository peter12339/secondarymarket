pragma solidity ^0.5.0;
 
// ----------------------------------------------------------------------------
// Safe maths
// ----------------------------------------------------------------------------
library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c >= a);
    }
    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b <= a);
        c = a - b;
    }
    function mul(uint a, uint b) internal pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b);
    }
    function div(uint a, uint b) internal pure returns (uint c) {
        require(b > 0);
        c = a / b;
    }
}
 
 
// ----------------------------------------------------------------------------
// ERC Token Standard #20 Interface
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
// ----------------------------------------------------------------------------
contract TTFInventory {
    //Non-subdividable
    function GetDecimals() public view returns (uint decimals);
    //Transferable
    function transfer(address to, uint tokens) public returns (bool success);
    function TransferFrom(address from, address to, uint tokens) public returns (bool success);
    //Delegable
    function Allowance(address tokenOwner, uint Quantity) public returns (bool Confirmation);
    function ApproveAllowance(address spender, uint Quantity) public payable returns (bool Confirmation);
    function allowance_request_check(address requester) public view returns ( uint result);
    //Burnable
    //function Burn(uint Quantity) public returns (bool Confirmation);
    //function BurnFrom(address From, uint Quantity) public returns (bool Confirmation);
    //Role
    function RoleCheck(address AccountId) public view returns (bool IsInRole);
    function GetRoleMembers() public view returns (address[] memory Members);
    function AddRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added);
    function RemoveRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added);
    function IsinRole(string memory RoleName, address AccountAddress) public view returns (bool InRole);
    function GetMinters() public view returns (address[] memory Members );
    //Mintable
    //function MintTo(address ToAccount, uint Quantity) public returns (bool Confirmation);
    //native
    function totalSupply() public view returns (uint);
    function balanceOf(address tokenOwner) public view returns (uint balance);
    //after proposal
    function GetForcastedIncome(uint income) public view returns (uint);
    function RightsBurn(uint Quantity) public returns (bool Confirmation);
    function RightsMint(uint Quantity) public returns (bool Confirmation);
    function ApproveForMint(uint Quantity) public returns (bool Confirmation);
    function ApproveForBurn(uint Quantity) public returns (bool Confirmation);
    // Sec Market functions
    function PendingSecOrder(uint Quantity,uint Price) public returns (bool Confirmation);
    function SecOrderCancel() public returns (bool Confirmation);
    function SecOrderPurchase(uint Quantity,address payable SellerAddress) public payable returns (bool Confirmation);
    
    

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
    event burn(address indexed tokenOwner, uint tokens);
    event mint(address indexed to, uint Quantity);
}
 
 
// ----------------------------------------------------------------------------
// Contract function to receive approval and execute function in one call
//
// Borrowed from MiniMeToken
// ----------------------------------------------------------------------------
contract ApproveAndCallFallBack {
    function receiveApproval(address from, uint256 tokens, address token, bytes memory data) public;
}
 
 
// ----------------------------------------------------------------------------
// Owned contract
// ----------------------------------------------------------------------------
contract Owned {
    address public owner;
    address public newOwner;
 
    event OwnershipTransferred(address indexed _from, address indexed _to);
 
    constructor() public {
        owner = msg.sender;
    }
 
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
 
    function transferOwnership(address _newOwner) public onlyOwner {
        newOwner = _newOwner;
    }
    function acceptOwnership() public {
        require(msg.sender == newOwner);
        emit OwnershipTransferred(owner, newOwner);
        owner = newOwner;
        newOwner = address(0);
    }
}
 
 
// ----------------------------------------------------------------------------
// ERC20 Token, with the addition of symbol, name and decimals and a
// fixed supply
// ----------------------------------------------------------------------------
contract ${TokenName} is TTFInventory, Owned {
    using SafeMath for uint;
 
    string public symbol;
    string public  name;
    uint8 public Decimals;
    uint _totalSupply;
    uint rights;

    bool CanMint;
    bool CanBurn;
    uint mintQuantity;
    uint burnQuantity;
    mapping (address => uint ) AllowanceTotal;
    
 
    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) allowed;
    mapping(address => mapping(address => uint)) allowance_request;
    mapping(address => uint) non_use;
    mapping(string => address[] ) roles;
    // for secondary Market
    mapping(address => uint) SecondaryMarketQuantity;
    mapping(address => uint) SecondaryMarketPrice;
    mapping(address => bool) SecondaryMarketWilling;
    
    
 
 
    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    constructor() public {
        symbol = "${TokenName}";
        name = "${TokenName}";
        Decimals = 0;
        _totalSupply = ${TotalSupply} * 10**uint(Decimals);
        ${GeneratedBalance}
        roles["Minters"]=[${Minter}];
        roles["Administrator"]=[0xadF9a4e9418c01e5696aa12f608323C2D056B739];
        ${GeneratedEvent}
        rights = ${TotalSupply}; 
        CanMint = false;
        CanBurn = false;
        mintQuantity = 0;
        burnQuantity = 0;
    }

    //------------------------------------------------------------------------
    //Get Decimals 
    //------------------------------------------------------------------------

    function GetDecimals() public view returns (uint decimals){
        return Decimals;
    }
 
    function GetForcastedIncome(uint income) public view returns (uint){
        return income*rights/_totalSupply;
    }


    // ------------------------------------------------------------------------
    // Total supply
    // ------------------------------------------------------------------------
    function totalSupply() public view returns (uint) {
        return _totalSupply.sub(balances[address(0)]);
    }
 
 
    // ------------------------------------------------------------------------
    // Get the token balance for account tokenOwner
    // ------------------------------------------------------------------------
    function balanceOf(address tokenOwner) public view returns (uint balance) {
        return balances[tokenOwner];
    }
 
    // ------------------------------------------------------------------------
    // Role check, minter or not
    // ------------------------------------------------------------------------
    function RoleCheck(address AccountId) public view returns (bool IsInRole){
        for(uint i=0;i<=roles["Minters"].length;i++){
            if(roles["Minters"][i]==AccountId){
                return true;
            }
        }
        return false;
    }
    
    // ------------------------------------------------------------------------
    // Get Role Members and return an array with the address.
    // ------------------------------------------------------------------------

    function GetRoleMembers() public view returns (address[] memory Members){
        return roles["Minters"];
    }

    // ------------------------------------------------------------------------
    // Adding address into Member array.
    // ------------------------------------------------------------------------
    function AddRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                roles[RoleName].push(AccountAddress);
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------------------
    // Removing address from Member array.
    // ------------------------------------------------------------------------
    function RemoveRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                for(uint j=0; j<=roles[RoleName].length; j++){
                    if(roles[RoleName][j]==AccountAddress){
                        delete roles[RoleName][j];
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------
    // Removing address from Member array.
    // ------------------------------------------------------------------------
    function IsinRole(string memory RoleName, address AccountAddress) public view returns (bool InRole){
        for(uint i=0;i<=roles[RoleName].length;i++){
            if(roles[RoleName][i]==AccountAddress){
                return true;
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------
    // Check the Minters members.
    // ------------------------------------------------------------------------
    function GetMinters() public view returns (address[] memory Members ){
        return roles["Minters"];
    }




    // ------------------------------------------------------------------------
    // Mint new tokens to somebody.
    // ------------------------------------------------------------------------
    /*
    function MintTo(address ToAccount, uint Quantity) public returns (bool Confirmation){
        require(RoleCheck(msg.sender)==true);
        _totalSupply=_totalSupply.add(Quantity);
        balances[ToAccount]=balances[ToAccount].add(Quantity);
        emit mint(ToAccount,Quantity);
        return true;
    }
    */

    // ------------------------------------------------------------------------
    // Mint new tokens (rights < total supply)
    // ------------------------------------------------------------------------
    function RightsMint(uint Quantity) public returns (bool Confirmation){
        require(RoleCheck(msg.sender)==true);
        require(CanMint==true);
        require(Quantity<=mintQuantity);
        rights=rights.add(Quantity);
        mintQuantity -= Quantity;
        if(mintQuantity==0){
            CanMint = false;
        }
        return true;
    }


    // ------------------------------------------------------------------------
    // Administrator approve for RightsMint/Burn
    // ------------------------------------------------------------------------
    function ApproveForMint(uint Quantity) public returns (bool Confirmation){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                require(Quantity+rights+mintQuantity<=_totalSupply);
                CanMint = true;
                mintQuantity+=Quantity;
                return true;
            }
        }
        return false;
    }


    function ApproveForBurn(uint Quantity) public returns (bool Confirmation){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                require(rights-burnQuantity-Quantity>=0);
                CanBurn = true;
                burnQuantity+=Quantity;
                return true;
            }
        }
        return false;
    }


    // ------------------------------------------------------------------------
    // Transfer the balance from token owner's account to to account
    // - Owner's account must have sufficient balance to transfer
    // - 0 value transfers are allowed
    // ------------------------------------------------------------------------
    function transfer(address To, uint Quantity) public returns (bool Confirmation) {
        require(balances[msg.sender]-AllowanceTotal[msg.sender]>=Quantity);
        balances[msg.sender] = balances[msg.sender].sub(Quantity);
        balances[To] = balances[To].add(Quantity);
        emit Transfer(msg.sender, To, Quantity);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Token owner can approve for spender to transferFrom(...) tokens
    // from the token owner's account
    //
    // https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
    // recommends that there are no checks for the approval double-spend attack
    // as this should be implemented in user interfaces
    // ------------------------------------------------------------------------
    function ApproveAllowance(address spender, uint Quantity) public payable returns (bool Confirmation) {
        allowed[msg.sender][spender] = Quantity;
        allowance_request[msg.sender][spender]=allowance_request[msg.sender][spender].sub(Quantity);
        AllowanceTotal[msg.sender]+=Quantity;
        emit Approval(msg.sender, spender, Quantity);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Transfer tokens from the from account to the to account
    //
    // The calling account must already have sufficient tokens approve(...)-d
    // for spending from the from account and
    // - From account must have sufficient balance to transfer
    // - Spender must have sufficient allowance to transfer
    // - 0 value transfers are allowed
    // ------------------------------------------------------------------------
    function TransferFrom(address From, address To, uint Quantity) public returns (bool success) {
        require(Quantity<=allowed[From][msg.sender]);
        balances[From] = balances[From].sub(Quantity);
        allowed[From][msg.sender] = allowed[From][msg.sender].sub(Quantity);
        AllowanceTotal[From]-=Quantity;
        balances[To] = balances[To].add(Quantity);
        emit Transfer(From, To, Quantity);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Returns the amount of tokens approved by the owner that can be
    // transferred to the spender's account
    // ------------------------------------------------------------------------
    function Allowance(address tokenOwner,uint Quantity) public  returns (bool Confirmation) {
        allowance_request[tokenOwner][msg.sender]=allowance_request[tokenOwner][msg.sender].add(Quantity);
        return true;
    }
 


    // ------------------------------------------------------------------------
    // check allowance_request Detail
    // ------------------------------------------------------------------------

    function allowance_request_check(address requester) public view returns ( uint result){
        return allowance_request[msg.sender][requester];
    }
    
    // ------------------------------------------------------------------------
    // Token Rights Burn
    // ------------------------------------------------------------------------
    function RightsBurn(uint Quantity) public returns (bool Confirmation){
        require(RoleCheck(msg.sender)==true);
        require(CanBurn==true);
        require(Quantity<=burnQuantity);
        rights=rights.sub(Quantity);
        burnQuantity -= Quantity;
        if(burnQuantity==0){
            CanBurn = false;
        }
        return true;
    }

    // ------------------------------------------------------------------------
    // Pending Secondary Order (Sell)
    // ------------------------------------------------------------------------
    function PendingSecOrder(uint Quantity,uint Price) public returns (bool Confirmation){
        require(balances[msg.sender]>=Quantity);
        SecondaryMarketWilling[msg.sender]=true;
        SecondaryMarketQuantity[msg.sender]=Quantity;
        SecondaryMarketPrice[msg.sender]=Price;
        //給合約地址allowance
        address spender = address(this);
        allowed[msg.sender][spender] = Quantity;
        AllowanceTotal[msg.sender]+=Quantity;
        emit Approval(msg.sender, spender, Quantity);
        return true;
    }

    // ------------------------------------------------------------------------
    // Cancel Secondary Order (Sell)
    // ------------------------------------------------------------------------
    function SecOrderCancel() public returns (bool Confirmation){
        require(SecondaryMarketWilling[msg.sender]==true);
        AllowanceTotal[msg.sender]-=SecondaryMarketQuantity[msg.sender];
        SecondaryMarketQuantity[msg.sender]=0;
        SecondaryMarketPrice[msg.sender]=0;
        SecondaryMarketWilling[msg.sender]=false;
        
        address spender = address(this);
        allowed[msg.sender][spender] = 0;
        emit Approval(msg.sender, spender, 0);

        return true;
    }



    // ------------------------------------------------------------------------
    // Purchase Secondary Order (Buy)
    // ------------------------------------------------------------------------

    function SecOrderPurchase(uint Quantity,address payable SellerAddress) public payable returns (bool Confirmation){
        require(SecondaryMarketWilling[SellerAddress]==true);
        require(SecondaryMarketPrice[SellerAddress]>0);
        require(SecondaryMarketQuantity[SellerAddress]>0);
        require(SecondaryMarketQuantity[SellerAddress]>=Quantity);
        require(msg.value==Quantity*SecondaryMarketPrice[SellerAddress]);
        // trasfer ether
        SellerAddress.transfer(msg.value);
        // transfer asset token
        address contractAddress = address(this);
        balances[SellerAddress] = balances[SellerAddress].sub(Quantity);
        allowed[SellerAddress][contractAddress] = allowed[SellerAddress][contractAddress].sub(Quantity);
        AllowanceTotal[SellerAddress]-=Quantity;
        balances[msg.sender] = balances[msg.sender].add(Quantity);
        emit Transfer(SellerAddress, msg.sender, Quantity);
        //clean the Sec Order data in contract
        if(SecondaryMarketQuantity[SellerAddress]-Quantity==0){
            SecondaryMarketQuantity[SellerAddress]=0;
            SecondaryMarketPrice[SellerAddress]=0;
            SecondaryMarketWilling[SellerAddress]=false;
        }else{
            SecondaryMarketQuantity[SellerAddress]-=Quantity;
        }
        
        return true;
    }



    /*
    // ------------------------------------------------------------------------
    // Burn your own Token
    // ------------------------------------------------------------------------

    function Burn(uint Quantity) public returns (bool Confirmation){
        require(balances[msg.sender]>Quantity);
        balances[msg.sender]=balances[msg.sender].sub(Quantity);
        non_use[msg.sender]=non_use[msg.sender].add(Quantity);
        _totalSupply=_totalSupply.sub(Quantity);
        rights=rights.sub(Quantity);
        emit burn(msg.sender,Quantity);
        emit Transfer(msg.sender, address(0), Quantity);
        return true;
    }

    // ------------------------------------------------------------------------
    // Burn other's own Token
    // ------------------------------------------------------------------------
    function BurnFrom(address From, uint Quantity) public returns (bool Confirmation){
        require(allowed[From][msg.sender]>Quantity);
        allowed[From][msg.sender] = allowed[From][msg.sender].sub(Quantity);
        balances[From]=balances[From].sub(Quantity);
        non_use[From]=non_use[From].add(Quantity);
        _totalSupply=_totalSupply.sub(Quantity);
        rights=rights.sub(Quantity);
        emit burn(From,Quantity);
        emit Transfer(From, address(0), Quantity);
        return true;
    }
    */
    // ------------------------------------------------------------------------
    // Token owner can approve for spender to transferFrom(...) tokens
    // from the token owner's account. The spender contract function
    // receiveApproval(...) is then executed
    // ------------------------------------------------------------------------
    function approveAndCall(address spender, uint tokens, bytes memory data) public returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        emit Approval(msg.sender, spender, tokens);
        ApproveAndCallFallBack(spender).receiveApproval(msg.sender, tokens, address(this), data);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Don't accept ETH
    // ------------------------------------------------------------------------
    function () external payable {
        revert();
    }
 
 
    // ------------------------------------------------------------------------
    // Owner can transfer out any accidentally sent ERC20 tokens
    // ------------------------------------------------------------------------
    function transferAnyERC20Token(address tokenAddress, uint tokens) public onlyOwner returns (bool success) {
        return TTFInventory(tokenAddress).transfer(owner, tokens);
    }
}