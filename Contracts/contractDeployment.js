var Web3 = require('web3');
var  fs = require('fs');
var Tx = require('ethereumjs-tx').Transaction;
const testnet = 'https://ropsten.infura.io/v3/a541e066f03d4654b032bf76f1442979';
const web3 = new Web3(new Web3.providers.HttpProvider(testnet));
const solc = require('solc');
filePath="./Contracts/InventoryTTF.sol";
user_key="c5ed0f82ffe696a5d160f34315d55e27a1d179fe31dbb614a1c24f36ec2b9577"
var wallet_address = "0xadF9a4e9418c01e5696aa12f608323C2D056B739";


function BalanceGen(owner,balance){
  temp1 = ""
  for(var i=0;i<owner.length;i++){
    temp = "balances["+owner[i]+"] = "+balance[i]+";\n"
    temp1=temp1+temp;
  }
  return temp1;
}


function EventGen(owner,balance){
  temp1 = ""
  for(var i=0;i<owner.length;i++){
    temp = "emit Transfer(address(0),"+owner[i]+", "+balance[i]+");\n"
    temp1 = temp1+temp;
  }
  return temp1;
}

function TotalGen(balance){
  temp = 0;
  for(i=0;i<balance.length;i++){
    temp += parseInt(balance[i]); 
  }
  return temp;
}







const Deployment = (db,symbol,TokenName,Minter,owner,balance) =>{

//fmuser addr  0x46C05Bb441F179d2f8429f05b808d2C168d54Ae3
//fmuser pk    eb64126339813f5daae5394eca9d34b6aeea7eb5f0498011b5c0e9b58b18abe9




GeneratedBalance=BalanceGen(owner,balance);
GeneratedEvent=EventGen(owner,balance);
TotalSupply = TotalGen(balance);



solfile=`pragma solidity ^0.5.0;
 
// ----------------------------------------------------------------------------
// Safe maths
// ----------------------------------------------------------------------------
library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c >= a);
    }
    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b <= a);
        c = a - b;
    }
    function mul(uint a, uint b) internal pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b);
    }
    function div(uint a, uint b) internal pure returns (uint c) {
        require(b > 0);
        c = a / b;
    }
}
 
 
// ----------------------------------------------------------------------------
// ERC Token Standard #20 Interface
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
// ----------------------------------------------------------------------------
contract TTFInventory {
    //Non-subdividable
    function GetDecimals() public view returns (uint decimals);
    //Transferable
    function transfer(address to, uint tokens) public returns (bool success);
    function TransferFrom(address from, address to, uint tokens) public returns (bool success);
    //Delegable
    function Allowance(address tokenOwner, uint Quantity) public returns (bool Confirmation);
    function ApproveAllowance(address spender, uint Quantity) public payable returns (bool Confirmation);
    function allowance_request_check(address requester) public view returns ( uint result);
    //Burnable
    //function Burn(uint Quantity) public returns (bool Confirmation);
    //function BurnFrom(address From, uint Quantity) public returns (bool Confirmation);
    //Role
    function RoleCheck(address AccountId) public view returns (bool IsInRole);
    function GetRoleMembers() public view returns (address[] memory Members);
    function AddRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added);
    function RemoveRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added);
    function IsinRole(string memory RoleName, address AccountAddress) public view returns (bool InRole);
    function GetMinters() public view returns (address[] memory Members );
    //Mintable
    //function MintTo(address ToAccount, uint Quantity) public returns (bool Confirmation);
    //native
    function totalSupply() public view returns (uint);
    function balanceOf(address tokenOwner) public view returns (uint balance);
    //after proposal
    function GetForcastedIncome(uint income) public view returns (uint);
    function RightsBurn(uint Quantity) public returns (bool Confirmation);
    function RightsMint(uint Quantity) public returns (bool Confirmation);
    function ApproveForMint(uint Quantity) public returns (bool Confirmation);
    function ApproveForBurn(uint Quantity) public returns (bool Confirmation);
    // Sec Market functions
    function PendingSecOrder(uint Quantity,uint Price) public returns (bool Confirmation);
    function SecOrderCancel() public returns (bool Confirmation);
    function SecOrderPurchase(uint Quantity,address payable SellerAddress) public payable returns (bool Confirmation);
    
    

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
    event burn(address indexed tokenOwner, uint tokens);
    event mint(address indexed to, uint Quantity);
}
 
 
// ----------------------------------------------------------------------------
// Contract function to receive approval and execute function in one call
//
// Borrowed from MiniMeToken
// ----------------------------------------------------------------------------
contract ApproveAndCallFallBack {
    function receiveApproval(address from, uint256 tokens, address token, bytes memory data) public;
}
 
 
// ----------------------------------------------------------------------------
// Owned contract
// ----------------------------------------------------------------------------
contract Owned {
    address public owner;
    address public newOwner;
 
    event OwnershipTransferred(address indexed _from, address indexed _to);
 
    constructor() public {
        owner = msg.sender;
    }
 
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }
 
    function transferOwnership(address _newOwner) public onlyOwner {
        newOwner = _newOwner;
    }
    function acceptOwnership() public {
        require(msg.sender == newOwner);
        emit OwnershipTransferred(owner, newOwner);
        owner = newOwner;
        newOwner = address(0);
    }
}
 
 
// ----------------------------------------------------------------------------
// ERC20 Token, with the addition of symbol, name and decimals and a
// fixed supply
// ----------------------------------------------------------------------------
contract ${TokenName} is TTFInventory, Owned {
    using SafeMath for uint;
 
    string public symbol;
    string public  name;
    uint8 public Decimals;
    uint _totalSupply;
    uint rights;

    bool CanMint;
    bool CanBurn;
    uint mintQuantity;
    uint burnQuantity;
    mapping (address => uint ) AllowanceTotal;
    
 
    mapping(address => uint) balances;
    mapping(address => mapping(address => uint)) allowed;
    mapping(address => mapping(address => uint)) allowance_request;
    mapping(address => uint) non_use;
    mapping(string => address[] ) roles;
    // for secondary Market
    mapping(address => uint) SecondaryMarketQuantity;
    mapping(address => uint) SecondaryMarketPrice;
    mapping(address => bool) SecondaryMarketWilling;
    
    
 
 
    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    constructor() public {
        symbol = "${TokenName}";
        name = "${TokenName}";
        Decimals = 0;
        _totalSupply = ${TotalSupply} * 10**uint(Decimals);
        ${GeneratedBalance}
        roles["Minters"]=[${Minter}];
        roles["Administrator"]=[0xadF9a4e9418c01e5696aa12f608323C2D056B739];
        ${GeneratedEvent}
        rights = ${TotalSupply}; 
        CanMint = false;
        CanBurn = false;
        mintQuantity = 0;
        burnQuantity = 0;
    }

    //------------------------------------------------------------------------
    //Get Decimals 
    //------------------------------------------------------------------------

    function GetDecimals() public view returns (uint decimals){
        return Decimals;
    }
 
    function GetForcastedIncome(uint income) public view returns (uint){
        return income*rights/_totalSupply;
    }


    // ------------------------------------------------------------------------
    // Total supply
    // ------------------------------------------------------------------------
    function totalSupply() public view returns (uint) {
        return _totalSupply.sub(balances[address(0)]);
    }
 
 
    // ------------------------------------------------------------------------
    // Get the token balance for account tokenOwner
    // ------------------------------------------------------------------------
    function balanceOf(address tokenOwner) public view returns (uint balance) {
        return balances[tokenOwner];
    }
 
    // ------------------------------------------------------------------------
    // Role check, minter or not
    // ------------------------------------------------------------------------
    function RoleCheck(address AccountId) public view returns (bool IsInRole){
        for(uint i=0;i<=roles["Minters"].length;i++){
            if(roles["Minters"][i]==AccountId){
                return true;
            }
        }
        return false;
    }
    
    // ------------------------------------------------------------------------
    // Get Role Members and return an array with the address.
    // ------------------------------------------------------------------------

    function GetRoleMembers() public view returns (address[] memory Members){
        return roles["Minters"];
    }

    // ------------------------------------------------------------------------
    // Adding address into Member array.
    // ------------------------------------------------------------------------
    function AddRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                roles[RoleName].push(AccountAddress);
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------------------
    // Removing address from Member array.
    // ------------------------------------------------------------------------
    function RemoveRoleMember(string memory RoleName, address AccountAddress) public returns (bool Added){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                for(uint j=0; j<=roles[RoleName].length; j++){
                    if(roles[RoleName][j]==AccountAddress){
                        delete roles[RoleName][j];
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------
    // Removing address from Member array.
    // ------------------------------------------------------------------------
    function IsinRole(string memory RoleName, address AccountAddress) public view returns (bool InRole){
        for(uint i=0;i<=roles[RoleName].length;i++){
            if(roles[RoleName][i]==AccountAddress){
                return true;
            }
        }
        return false;
    }

    // ------------------------------------------------------------------------
    // Check the Minters members.
    // ------------------------------------------------------------------------
    function GetMinters() public view returns (address[] memory Members ){
        return roles["Minters"];
    }




    // ------------------------------------------------------------------------
    // Mint new tokens to somebody.
    // ------------------------------------------------------------------------
    /*
    function MintTo(address ToAccount, uint Quantity) public returns (bool Confirmation){
        require(RoleCheck(msg.sender)==true);
        _totalSupply=_totalSupply.add(Quantity);
        balances[ToAccount]=balances[ToAccount].add(Quantity);
        emit mint(ToAccount,Quantity);
        return true;
    }
    */

    // ------------------------------------------------------------------------
    // Mint new tokens (rights < total supply)
    // ------------------------------------------------------------------------
    function RightsMint(uint Quantity) public returns (bool Confirmation){
        require(RoleCheck(msg.sender)==true);
        require(CanMint==true);
        require(Quantity<=mintQuantity);
        rights=rights.add(Quantity);
        mintQuantity -= Quantity;
        if(mintQuantity==0){
            CanMint = false;
        }
        return true;
    }


    // ------------------------------------------------------------------------
    // Administrator approve for RightsMint/Burn
    // ------------------------------------------------------------------------
    function ApproveForMint(uint Quantity) public returns (bool Confirmation){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                require(Quantity+rights+mintQuantity<=_totalSupply);
                CanMint = true;
                mintQuantity+=Quantity;
                return true;
            }
        }
        return false;
    }


    function ApproveForBurn(uint Quantity) public returns (bool Confirmation){
        for(uint i=0;i<=roles["Administrator"].length;i++){
            if(roles["Administrator"][i]==msg.sender){
                require(rights-burnQuantity-Quantity>=0);
                CanBurn = true;
                burnQuantity+=Quantity;
                return true;
            }
        }
        return false;
    }


    // ------------------------------------------------------------------------
    // Transfer the balance from token owner's account to to account
    // - Owner's account must have sufficient balance to transfer
    // - 0 value transfers are allowed
    // ------------------------------------------------------------------------
    function transfer(address To, uint Quantity) public returns (bool Confirmation) {
        require(balances[msg.sender]-AllowanceTotal[msg.sender]>=Quantity);
        balances[msg.sender] = balances[msg.sender].sub(Quantity);
        balances[To] = balances[To].add(Quantity);
        emit Transfer(msg.sender, To, Quantity);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Token owner can approve for spender to transferFrom(...) tokens
    // from the token owner's account
    //
    // https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20-token-standard.md
    // recommends that there are no checks for the approval double-spend attack
    // as this should be implemented in user interfaces
    // ------------------------------------------------------------------------
    function ApproveAllowance(address spender, uint Quantity) public payable returns (bool Confirmation) {
        allowed[msg.sender][spender] = Quantity;
        allowance_request[msg.sender][spender]=allowance_request[msg.sender][spender].sub(Quantity);
        AllowanceTotal[msg.sender]+=Quantity;
        emit Approval(msg.sender, spender, Quantity);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Transfer tokens from the from account to the to account
    //
    // The calling account must already have sufficient tokens approve(...)-d
    // for spending from the from account and
    // - From account must have sufficient balance to transfer
    // - Spender must have sufficient allowance to transfer
    // - 0 value transfers are allowed
    // ------------------------------------------------------------------------
    function TransferFrom(address From, address To, uint Quantity) public returns (bool success) {
        require(Quantity<=allowed[From][msg.sender]);
        balances[From] = balances[From].sub(Quantity);
        allowed[From][msg.sender] = allowed[From][msg.sender].sub(Quantity);
        AllowanceTotal[From]-=Quantity;
        balances[To] = balances[To].add(Quantity);
        emit Transfer(From, To, Quantity);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Returns the amount of tokens approved by the owner that can be
    // transferred to the spender's account
    // ------------------------------------------------------------------------
    function Allowance(address tokenOwner,uint Quantity) public  returns (bool Confirmation) {
        allowance_request[tokenOwner][msg.sender]=allowance_request[tokenOwner][msg.sender].add(Quantity);
        return true;
    }
 


    // ------------------------------------------------------------------------
    // check allowance_request Detail
    // ------------------------------------------------------------------------

    function allowance_request_check(address requester) public view returns ( uint result){
        return allowance_request[msg.sender][requester];
    }
    
    // ------------------------------------------------------------------------
    // Token Rights Burn
    // ------------------------------------------------------------------------
    function RightsBurn(uint Quantity) public returns (bool Confirmation){
        require(RoleCheck(msg.sender)==true);
        require(CanBurn==true);
        require(Quantity<=burnQuantity);
        rights=rights.sub(Quantity);
        burnQuantity -= Quantity;
        if(burnQuantity==0){
            CanBurn = false;
        }
        return true;
    }

    // ------------------------------------------------------------------------
    // Pending Secondary Order (Sell)
    // ------------------------------------------------------------------------
    function PendingSecOrder(uint Quantity,uint Price) public returns (bool Confirmation){
        require(balances[msg.sender]>=Quantity);
        SecondaryMarketWilling[msg.sender]=true;
        SecondaryMarketQuantity[msg.sender]=Quantity;
        SecondaryMarketPrice[msg.sender]=Price;
        //給合約地址allowance
        address spender = address(this);
        allowed[msg.sender][spender] = Quantity;
        AllowanceTotal[msg.sender]+=Quantity;
        emit Approval(msg.sender, spender, Quantity);
        return true;
    }

    // ------------------------------------------------------------------------
    // Cancel Secondary Order (Sell)
    // ------------------------------------------------------------------------
    function SecOrderCancel() public returns (bool Confirmation){
        require(SecondaryMarketWilling[msg.sender]==true);
        AllowanceTotal[msg.sender]-=SecondaryMarketQuantity[msg.sender];
        SecondaryMarketQuantity[msg.sender]=0;
        SecondaryMarketPrice[msg.sender]=0;
        SecondaryMarketWilling[msg.sender]=false;
        
        address spender = address(this);
        allowed[msg.sender][spender] = 0;
        emit Approval(msg.sender, spender, 0);

        return true;
    }



    // ------------------------------------------------------------------------
    // Purchase Secondary Order (Buy)
    // ------------------------------------------------------------------------

    function SecOrderPurchase(uint Quantity,address payable SellerAddress) public payable returns (bool Confirmation){
        require(SecondaryMarketWilling[SellerAddress]==true);
        require(SecondaryMarketPrice[SellerAddress]>0);
        require(SecondaryMarketQuantity[SellerAddress]>0);
        require(SecondaryMarketQuantity[SellerAddress]>=Quantity);
        require(msg.value==Quantity*SecondaryMarketPrice[SellerAddress]);
        // trasfer ether
        SellerAddress.transfer(msg.value);
        // transfer asset token
        address contractAddress = address(this);
        balances[SellerAddress] = balances[SellerAddress].sub(Quantity);
        allowed[SellerAddress][contractAddress] = allowed[SellerAddress][contractAddress].sub(Quantity);
        AllowanceTotal[SellerAddress]-=Quantity;
        balances[msg.sender] = balances[msg.sender].add(Quantity);
        emit Transfer(SellerAddress, msg.sender, Quantity);
        //clean the Sec Order data in contract
        if(SecondaryMarketQuantity[SellerAddress]-Quantity==0){
            SecondaryMarketQuantity[SellerAddress]=0;
            SecondaryMarketPrice[SellerAddress]=0;
            SecondaryMarketWilling[SellerAddress]=false;
        }else{
            SecondaryMarketQuantity[SellerAddress]-=Quantity;
        }
        
        return true;
    }



    /*
    // ------------------------------------------------------------------------
    // Burn your own Token
    // ------------------------------------------------------------------------

    function Burn(uint Quantity) public returns (bool Confirmation){
        require(balances[msg.sender]>Quantity);
        balances[msg.sender]=balances[msg.sender].sub(Quantity);
        non_use[msg.sender]=non_use[msg.sender].add(Quantity);
        _totalSupply=_totalSupply.sub(Quantity);
        rights=rights.sub(Quantity);
        emit burn(msg.sender,Quantity);
        emit Transfer(msg.sender, address(0), Quantity);
        return true;
    }

    // ------------------------------------------------------------------------
    // Burn other's own Token
    // ------------------------------------------------------------------------
    function BurnFrom(address From, uint Quantity) public returns (bool Confirmation){
        require(allowed[From][msg.sender]>Quantity);
        allowed[From][msg.sender] = allowed[From][msg.sender].sub(Quantity);
        balances[From]=balances[From].sub(Quantity);
        non_use[From]=non_use[From].add(Quantity);
        _totalSupply=_totalSupply.sub(Quantity);
        rights=rights.sub(Quantity);
        emit burn(From,Quantity);
        emit Transfer(From, address(0), Quantity);
        return true;
    }
    */
    // ------------------------------------------------------------------------
    // Token owner can approve for spender to transferFrom(...) tokens
    // from the token owner's account. The spender contract function
    // receiveApproval(...) is then executed
    // ------------------------------------------------------------------------
    function approveAndCall(address spender, uint tokens, bytes memory data) public returns (bool success) {
        allowed[msg.sender][spender] = tokens;
        emit Approval(msg.sender, spender, tokens);
        ApproveAndCallFallBack(spender).receiveApproval(msg.sender, tokens, address(this), data);
        return true;
    }
 
 
    // ------------------------------------------------------------------------
    // Don't accept ETH
    // ------------------------------------------------------------------------
    function () external payable {
        revert();
    }
 
 
    // ------------------------------------------------------------------------
    // Owner can transfer out any accidentally sent ERC20 tokens
    // ------------------------------------------------------------------------
    function transferAnyERC20Token(address tokenAddress, uint tokens) public onlyOwner returns (bool success) {
        return TTFInventory(tokenAddress).transfer(owner, tokens);
    }
}`








const source = {
  language: "Solidity",
  sources: {
      //'solFile': { content: fs.readFileSync(filePath, 'utf8')
      'solFile': { content: solfile
    }
  },
  settings: {
    outputSelection: {
      "*": {
        "*": [ "abi", "evm.bytecode" ]
      }
    }
  }
};


// Note: You have to pass the input in with JSON.stringify now.
//const compiledSol = JSON.parse(solc.compile(JSON.stringify(source)));
const compiledSol = JSON.parse(solc.compile(JSON.stringify(source)));
if(compiledSol.errors) {
  compiledSol.errors.forEach(err => console.error(err.formattedMessage));
}


// Note: This changed slightly since I'm using JSON.parse above.
var bytecode  = "0x"+compiledSol.contracts.solFile[TokenName].evm.bytecode.object;

var abi = compiledSol.contracts.solFile[TokenName].abi;




var count = web3.eth.getTransactionCount(wallet_address);
var data = bytecode
var rawTransaction = {
  "from": wallet_address,
  "nonce": web3.toHex(count),
  "gasPrice": web3.toHex(5000000000),
  "gasLimit": web3.toHex(5000000),
  "data": data
};

var privateKey = Buffer.from(user_key,"hex");// 此處需要連接個人的私鑰
var tx = new Tx(rawTransaction,{chain:"ropsten"});     
tx.sign(privateKey);


var serializedTx = tx.serialize();
web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), 
  async function(err, hash) {
    if (!err){
        console.log("https://ropsten.etherscan.io/tx/"+hash);
        //var new_contract_address = await getContractAddress(hash);
        db.query("UPDATE `demo`.`product` SET `eth_TxHash` = ? WHERE (`symbol` = ?);",[hash,symbol],function(err){
            if(err){
                console.log(err);
            }
            else{
                console.log("Successfully insert Tx Hash in db.")
            }
        })
    }
    else{
        console.log(err);
    }
        
});





};






module.exports = {
  Deployment
}
