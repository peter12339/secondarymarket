

var Web3 = require('web3');
const testnet = 'https://ropsten.infura.io/v3/a541e066f03d4654b032bf76f1442979';
const web3 = new Web3(new Web3.providers.HttpProvider(testnet));
var fm_address = "0xadF9a4e9418c01e5696aa12f608323C2D056B739"
var fm_pk = "c5ed0f82ffe696a5d160f34315d55e27a1d179fe31dbb614a1c24f36ec2b9577"

var ABI = [
	{
		"constant": false,
		"inputs": [],
		"name": "acceptOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "RoleName",
				"type": "string"
			},
			{
				"name": "AccountAddress",
				"type": "address"
			}
		],
		"name": "AddRoleMember",
		"outputs": [
			{
				"name": "Added",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "tokenOwner",
				"type": "address"
			},
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "Allowance",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "spender",
				"type": "address"
			},
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "ApproveAllowance",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "spender",
				"type": "address"
			},
			{
				"name": "tokens",
				"type": "uint256"
			},
			{
				"name": "data",
				"type": "bytes"
			}
		],
		"name": "approveAndCall",
		"outputs": [
			{
				"name": "success",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "ApproveForBurn",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "ApproveForMint",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "Quantity",
				"type": "uint256"
			},
			{
				"name": "Price",
				"type": "uint256"
			}
		],
		"name": "PendingSecOrder",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "RoleName",
				"type": "string"
			},
			{
				"name": "AccountAddress",
				"type": "address"
			}
		],
		"name": "RemoveRoleMember",
		"outputs": [
			{
				"name": "Added",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "RightsBurn",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "RightsMint",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [],
		"name": "SecOrderCancel",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "Quantity",
				"type": "uint256"
			},
			{
				"name": "SellerAddress",
				"type": "address"
			}
		],
		"name": "SecOrderPurchase",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "To",
				"type": "address"
			},
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "transfer",
		"outputs": [
			{
				"name": "Confirmation",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "tokenAddress",
				"type": "address"
			},
			{
				"name": "tokens",
				"type": "uint256"
			}
		],
		"name": "transferAnyERC20Token",
		"outputs": [
			{
				"name": "success",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "From",
				"type": "address"
			},
			{
				"name": "To",
				"type": "address"
			},
			{
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "TransferFrom",
		"outputs": [
			{
				"name": "success",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_newOwner",
				"type": "address"
			}
		],
		"name": "transferOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"payable": true,
		"stateMutability": "payable",
		"type": "fallback"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "_from",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "_to",
				"type": "address"
			}
		],
		"name": "OwnershipTransferred",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "from",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "tokens",
				"type": "uint256"
			}
		],
		"name": "Transfer",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "tokenOwner",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "spender",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "tokens",
				"type": "uint256"
			}
		],
		"name": "Approval",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "tokenOwner",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "tokens",
				"type": "uint256"
			}
		],
		"name": "burn",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"name": "Quantity",
				"type": "uint256"
			}
		],
		"name": "mint",
		"type": "event"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "requester",
				"type": "address"
			}
		],
		"name": "allowance_request_check",
		"outputs": [
			{
				"name": "result",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "tokenOwner",
				"type": "address"
			}
		],
		"name": "balanceOf",
		"outputs": [
			{
				"name": "balance",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "Decimals",
		"outputs": [
			{
				"name": "",
				"type": "uint8"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "GetDecimals",
		"outputs": [
			{
				"name": "decimals",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "income",
				"type": "uint256"
			}
		],
		"name": "GetForcastedIncome",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "GetMinters",
		"outputs": [
			{
				"name": "Members",
				"type": "address[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "GetRoleMembers",
		"outputs": [
			{
				"name": "Members",
				"type": "address[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "RoleName",
				"type": "string"
			},
			{
				"name": "AccountAddress",
				"type": "address"
			}
		],
		"name": "IsinRole",
		"outputs": [
			{
				"name": "InRole",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "name",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "newOwner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "AccountId",
				"type": "address"
			}
		],
		"name": "RoleCheck",
		"outputs": [
			{
				"name": "IsInRole",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "symbol",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalSupply",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]

async function RequestAllowance(contract_addr,quantity,token_owner){
	return new Promise(function (resolve,reject){
contractABI = ABI
		admin_addr = fm_address //平台addr
		var contract = web3.eth.contract(contractABI).at(contract_addr);
		var count = web3.eth.getTransactionCount(admin_addr); 
		var rawTransaction = {
			"from": admin_addr, //由平台發出allowance request
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.Allowance.getData(token_owner,quantity)//使用allowance request function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(fm_pk,"hex");// 此處需要連接個人的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Allowance Request Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});
	})
}



async function ApproveAllowanceRequest(contract_addr,spender,quantity,token_owner,ownerPK){
	return new Promise(function(resolve,reject){
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr);
		var count = web3.eth.getTransactionCount(token_owner); 
		var rawTransaction = {
			"from": token_owner, //由賣家發出allowance approve給平台方
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.ApproveAllowance.getData(spender,quantity)//使用approve allowance function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(ownerPK,"hex");// 此處需要連接個人的私鑰
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Allowance Approve Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});
	});

}

async function TransferFrom(contract_addr,transfer_from,transfer_to,quantity){//在他付費之後，平台代理轉token給買家
	return new Promise(function(resolve, reject){
		admin_addr = fm_address//平台address
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr);
		var count = web3.eth.getTransactionCount(admin_addr); 
		var rawTransaction = {
			"from": admin_addr, //由平台發出tranfer from給買方
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.TransferFrom.getData(transfer_from,transfer_to,quantity)//使用trnasfer from function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(fm_pk,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Transfer From Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});
	})
}








async function RightsBurn(owner_address,quantity,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.RightsBurn.getData(quantity)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Burn Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}







async function AddRoleMember(target_addr,role,contract_addr){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(fm_address); 
		var rawTransaction = {
			"from": fm_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.AddRoleMember.getData(role,target_addr)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(fm_pk,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Add "+role+" Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}

async function RemoveRoleMember(target_addr,role,contract_addr){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(fm_address); 
		var rawTransaction = {
			"from": fm_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.RemoveRoleMember.getData(role,target_addr)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(fm_pk,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Remove "+role+" Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}





async function getContractAddress(TxHash){
    return new Promise(async function (resolve,reject){
        try{
            var receipt =await web3.eth.getTransactionReceipt(TxHash);
	        console.log("contract address: "+receipt.logs[0].address);
            resolve(receipt.logs[0].address)
        }
        catch(err){
            reject(err);
        }
        
    })
}


// after proposal 
async function ApproveForBurn(owner_address,quantity,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.ApproveForBurn.getData(quantity)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Burn Approvement Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}



async function ApproveForMint(owner_address,quantity,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.ApproveForMint.getData(quantity)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Burn Approvement Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}



async function RightsMint(owner_address,quantity,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.RightsMint.getData(quantity)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Rights Mint Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}



async function PendingSecOrder(owner_address,price,quantity,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.PendingSecOrder.getData(quantity,price)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Rights Mint Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}

async function SecOrderPurchase(owner_address,seller_addr,price,quantity,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": price,
			"data": contract.SecOrderPurchase.getData(quantity,seller_addr)//Burn function
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Rights Mint Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}

async function SecOrderCancel(owner_address,contract_addr,PK){
	return new Promise(function(resolve, reject){
		try{
		contractABI = ABI
		var contract = web3.eth.contract(contractABI).at(contract_addr); 
		var count = web3.eth.getTransactionCount(owner_address); 
		var rawTransaction = {
			"from": owner_address, //由token owner觸發burn function
			"nonce": web3.toHex(count), //上面都是十六進位
			"gasPrice": web3.toHex(21000000000),
			"gasLimit": web3.toHex(200000),
			"to": contract_addr, //該token合約
			"value": "0x0",
			"data": contract.SecOrderCancel.getData()
		};
        var Tx = require("ethereumjs-tx").Transaction;
        var privateKey = Buffer.from(PK,"hex");// 此處需要連接平台的私鑰(平台PK)
        var tx = new Tx(rawTransaction,{chain:"ropsten"});     
        tx.sign(privateKey);
		var serializedTx = tx.serialize();//把它序列化
		web3.eth.sendRawTransaction('0x' + serializedTx.toString('hex'), function(err,hash){//call back function 上面做完才會往下，等待時就不會進入
			if(!err){
				console.log("https://ropsten.etherscan.io/tx/"+hash);
				console.log("Rights Mint Event Done!")
				resolve();
			}
			else{
				reject(err);
			}
		});}
		catch(err){
			reject(err);
		}
	})
}





module.exports={
	RequestAllowance:RequestAllowance,
	ApproveAllowanceRequest:ApproveAllowanceRequest,
	TransferFrom:TransferFrom,
	RightsMint:RightsMint,
	RightsBurn:RightsBurn,
	AddRoleMember:AddRoleMember,
	RemoveRoleMember:RemoveRoleMember,
	getContractAddress:getContractAddress,
	ApproveForBurn:ApproveForBurn,
	ApproveForMint:ApproveForMint,
	PendingSecOrder:PendingSecOrder,
	SecOrderPurchase:SecOrderPurchase,
	SecOrderCancel:SecOrderCancel
}
