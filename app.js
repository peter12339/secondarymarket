var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('mysql');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var orderRouter = require('./routes/order');
var registryRouter = require('./routes/registry');
var TokenInfoRouter = require('./routes/TokenInfo');
var SecondaryMarketRouter = require('./routes/SecondaryMarket')
var publisherRouter =require('./routes/publisher')
var backenduserRouter = require('./routes/backend_user')
var  session  = require('express-session')
var bodyparser = require('body-parser')


var con  = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'peter12339',
  database:"demo"
});

con.connect(function(err) {
if (err) {
    console.log('connecting error');
    return;
}
console.log('connecting success');
});


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// login session use
app.use(session({
  secret : 'secret', // 對session id 相關的cookie 進行簽名
  resave : true,
  saveUninitialized: false, // 是否儲存未初始化的會話
  cookie : {
  maxAge : 1000 * 60 * 3, // 設定 session 的有效時間，單位毫秒
  },
  }));

// db state
app.use(function(req, res, next) {
  req.con = con;
  next();
});

app.use('/', indexRouter);
app.use('/SecondaryMarket',SecondaryMarketRouter);
app.use('/login', loginRouter);
app.use('/users', usersRouter);
app.use('/order', orderRouter);
app.use('/registry', registryRouter);
app.use('/TokenInfo', TokenInfoRouter);
app.use('/publisher', publisherRouter);
app.use('/BackendUserIndex',backenduserRouter);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;
